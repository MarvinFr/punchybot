#!/bin/bash
source venv/bin/activate

while true
  do
    screen -S punchybot python -Xmx1024M -Xms512M punchybot.py
    sleep 5
done