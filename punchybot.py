from telegram import *
from telegram.ext import *
from time import sleep
from datetime import datetime, date
import json
import os.path

bot_token = '1643843425:AAEY7m_ZP8ycioNe0pFe84rB9MaWT4iP0Fs'
job_file_name = 'job_file.json'
job_file_path = '/Users/marvinfritz/Documents/Projekte/Telegram/' + job_file_name

job_list = {
    "jobs": [
        {
            "name": "Tische reinigen / Sidolin nachfuellen"
        },
        {
            "name": "Tische reinigen / Sidolin nachfuellen"
        },
        {
            "name": "Boden putzen"
        },
        {
            "name": "Boden putzen"
        },
        {
            "name": "Boden putzen"
        },
        {
            "name": "Boden putzen"
        },
        {
            "name": "Lichtdienst"
        },
        {
            "name": "Pflanzen gießen"
        },
        {
            "name": "Lichtdienst"
        },
        {
            "name": "Waschbecken reinigen"
        },
        {
            "name": "Fester schliesssen"
        },
    ]
}

name_list = {
    "names": [
        "Marc S.",
        "Adrian",
        "Benedict",
        "Enis",
        "Frank",
        "Lukas",
        "Musti",
        "Rosario",
        "Thomas",
        "Julian",
        "Leon",
        "Ertugrul",
        "Marcel",
        "Lilly",
        "Philip",
        "Christian",
        "Sefacan",
        "Dennis",
        "Marc R.",
        "Darko",
        "Selin",
        "Keyth",
        "Maik"
    ]
}


def send_message(punchy, message):
    """Send the alarm message."""
    punchy.send_message(chat_id='-573293351', text=message)


def send_jobs(punchy):
    if not os.path.isfile(job_file_path):
        with open(job_file_path, 'w') as job_file:
            json.dump(job_list, job_file)
            job_file.close()
            print("Die Job Datei wurde neu angelegt")
    else:
        print('Job file exists')
        job_file = open(job_file_name, "r")
        json_object = json.load(job_file)
        json_file.close()
        array_length = 0
        for job in json_object['jobs']:
            array_length += 1

        # Delete last element
        removed_job = json_object['jobs'][array_length - 1]

        del json_object['jobs'][array_length - 1]

        new_order = {
            "jobs": [

            ]
        }

        new_order['jobs'].append(removed_job)

        for job in json_object['jobs']:
            new_order['jobs'].append(job)

        os.remove(job_file_path)
        with open(job_file_path, 'w') as job_file:
            json.dump(new_order, job_file)
            job_file.close()
            print("Die Job Datei wurde neu angelegt")

        # print jobs
        print('rofl')
        job_file = open(job_file_name, "r")
        json_new_order = json.load(job_file)
        json_file.close()
        count = 0
        message_array = []
        for people in name_list['names']:
            print(people)
            # print(json_new_order['jobs'][count]['name'])
            message_array.append(people + " - " + json_new_order['jobs'][count]['name'])
            if count < array_length - 1:
                count += 1
            elif count == array_length - 1:
                count = 0

        jobs = '\n'.join(message_array)
        punchy.send_message(chat_id='-573293351', text=jobs)


punchy = Bot(bot_token)
updater = Updater(bot_token, use_context=True)
# dispatcher = updater.dispatcher

while True:
    dateTimeObj = datetime.now()
    timestampStr = dateTimeObj.strftime("%d_%m_%Y")

    json_file = open("date_log.json", "r")
    json_object = json.load(json_file)
    json_file.close()

    if json_object['last_log_message'] == timestampStr:
        # same day
        sleep(60 * 15)
    else:
        # new day
        now = datetime.now()
        if now.hour >= 13:
            send_jobs(punchy=punchy)
            with open('date_log.json', 'r+') as file:
                data = json.load(file)
                data['last_log_message'] = timestampStr  # <--- add `id` value.
                file.seek(0)  # <--- should reset file position to the beginning.
                json.dump(data, file, indent=4)
                file.truncate()  # remove remaining part

    updater.start_polling()

    sleep(60)

# updater.idle()
